## Movitrack ##

Aplicacion web movil usando [**HTML5**](http://dev.w3.org/html5/spec/), [**JQuery Mobile**](http://www.jquerymobile.com) y [**PHP**](http://www.php.net/) para ofrecer un informe de estado de movilidad vial, usando la **API** de [**Google Maps**](https://developers.google.com/maps/)

### Lista de Cambios ###

* Agregado nuevos iconos (gracias a [jQuery Mobile Icon Pack](http://andymatthews.net/))
* Mejoras en el uso del diseño de rutas (usando [**Google Maps API**](https://developers.google.com/maps/?hl=es))
* Nuevo tema ([gracias a JQuery Mobile themeroller](http://jquerymobile.com/themeroller/index.php))

### Licencia ###
[LGPL-2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html)

El texto completo de la licencia puede ser encontrado en el archivo **LGPL.txt**

### Contacto ###
[Diniremix on Github](https://github.com/diniremix)

email: *diniremix [at] gmail [dot] com*
