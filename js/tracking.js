$(document).ready(function() {
	var divMapa=document.getElementById('maptrack');
	var posicionActual,iLongitud,iLatitud;
	var objMapa,objCoordenadas,marker,titleMap;
	var zoomMap=15;
	var objOpciones={};

	function currentPosition () {
		console.log("intentanto establecer posicion...");	
		$.mobile.loadingMessageTextVisible = true;
		$.mobile.showPageLoadingMsg("c", "Cargando datos espere...", false);
		if(navigator.geolocation){		
			navigator.geolocation.getCurrentPosition(function(objPosicion){		
				iLongitud=objPosicion.coords.longitude, 
				iLatitud=objPosicion.coords.latitude;
				console.log('Posicion actual: Latitud: '+iLatitud+' - Longitud: '+iLongitud);
				
				objCoordenadas=new google.maps.LatLng(iLatitud,iLongitud);		
				objOpciones={
						mapTypeId:		google.maps.MapTypeId.ROADMAP,
						zoom: 			zoomMap,
						mapTypeControl:	true,
						center: 		objCoordenadas
				};

				objMapa=new google.maps.Map(divMapa,objOpciones);
				
				titlemap='La direccion es: '+'Latitud: '+iLatitud+' - Longitud: '+iLongitud;
				newmarker(titlemap,objCoordenadas,objMapa);

			},function(objError){
				console.log("Error al cargar el mapa");
				$("#errormap").popup("open",{transition:"slide"});
			});
		}else{
			console.log("Su navegador no soporta Geolocation API de HTML5");
			$("#errorapi").popup("open",{transition:"slide"});
		}
		$.mobile.hidePageLoadingMsg();
	}
	
	function newmarker(newTitle,newpos,newMap) {
		setTimeout( function(){
			marker=new google.maps.Marker({
				title:		newTitle,	
				position:	newpos,
				map:		newMap,
				draggable:true,
	    		animation: google.maps.Animation.DROP
			});
		},700);
	}

	function newmarkerWindow(contentString,newTitle,newpos,newMap) {
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});	

		setTimeout( function(){
			marker=new google.maps.Marker({
				title:		newTitle,	
				position:	newpos,
				map:		newMap,
				draggable:true,
	    		animation: google.maps.Animation.DROP
			});
		},700);

		setTimeout(function () {
			infowindow.open(newMap,marker);
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(newMap,marker);
			});
		},700);
	}

	function fullscreenMap() {
		setTimeout( function(){
			$("#maptrack").css("position", 'fixed').
			css('left', 0).
			css('top', 44).
			css("width", '100%').
			css("height", '100%');
			console.log("mapa a fullscreen");
			google.maps.event.trigger(divMapa, 'resize');
		},500);
	}

	function clearPosition() {
		console.log("limpiarUbicacion");
		ultimaPosicionUsuario = new google.maps.LatLng(0,0);
        if (marker){
        	console.log("borrando marcador");
            marker.setMap(null);
            marker = null;
        }else{
			console.log("no hay marker");
        }
	}

	function sendMessage () {
		console.log("enviar el msg",$("#textmsg").val());
		var userID="unknown";
		var usuario="unknown";
		if(localStorage.getItem("userID")){
			userID=localStorage.getItem("userID");
			usuario=localStorage.getItem("nombres");
			usuario+=' '+localStorage.getItem("apellidos");
		}
		console.log("ID del usuario",userID);

		var msg=$("#textmsg").val();
		var currentPos="no locate";			
		var fecha=fechaActual();
		if (iLatitud && iLongitud) {
			currentPos=iLatitud+","+iLongitud;
			$.ajax({
	          url: './clases/appcontroller.php',
	          method: 'get',
	          data: {option: "sendmsg", userid:userID,msg:msg,fecha:fecha,currentPos:currentPos,estado_: true, opt:1},
	          success: function(data) {
	              console.log("la data es:",data);
	            if (data==-1) {
	              console.log("Error al guardar");
	            }else{
					console.log("cargando la info combos:",data);
					clearPosition();
					var divinfo='<div id="content"><div id="siteNotice"></div><h3 id="firstHeading" class="firstHeading">'+usuario+' ('+currentPos+')</h3><div id="bodyContent"><p>'+msg+'</p><p><strong>Enviado:</strong><br> '+fecha+'</p></div></div>';
					newmarkerWindow(divinfo,msg,objCoordenadas,objMapa);
					
	            }
	          },
	          error:function(){
	            console.log("Error al establecer la conexion");
	          }
	    	});
		}
	}

	function fechaActual() {
		moment.lang("es");
		var hoy=moment().format('MMMM D [de] YYYY, h:mm:ss a');
		console.log("fecha y hora actual:",hoy);
		return hoy;
	}

/********************************************************************************/
	$("#getposition").click(function(){
		console.log("click en ubicarme");
		currentPosition();
	});

	$("#clear").click(function(){
		console.log("click en clear");
		clearPosition();
	});

	$("#sendmsg").click(function(){
		console.log("click en sendmsg");
		if ($("#textmsg").val()!="") {
			setTimeout( function(){
				$("#popupMsg").popup("close");
			},400);
			sendMessage();			
		}else{
			console.log("no esta lleno");
			$("#popupBasic").popup("open");
		}
	});

	$("#popupMsg").on({
		popupafterclose: function() {
			console.log("cerrando el popup");
			$("#textmsg").val("");
		}
	});

	$("#btn-login").click(function(){
		var logced=$("#logced").val();
		var status,userID;
		if(logced){
			$.ajax({
				url: './clases/appcontroller.php',
				method: 'get',
				data: {option: "login", logced:logced,estado_: true, opt:2},
				dataType:'json',
				success: function(data) {
					//console.log("data:",data);
					if (data[0].status) {
						if (data[0].status=="Ok") {
							$("#logced").val("");
							localStorage.setItem("userID",data[1].userID)
							localStorage.setItem("nombres",data[2].nombres)
							localStorage.setItem("apellidos",data[3].apellidos)
							$.mobile.changePage("#mappage",{transition: "slide"});
						}else{
							//console.log("Estado:",data[0].status);
							$("#logced").select("");
							$("#perror").popup("open");
						}
					}else{
						//console.log("No se recibieron los datos adecuados");
						$("#logced").val("");
						$("#perror").popup("open");
					};
				},
				error:function(){
					console.log("Error al establecer la conexion");
				}
			});
		}else{
			console.log("no hay logced");
		}
	});

	$("#logout").click(function(){
		localStorage.removeItem("userID");
		localStorage.removeItem("nombres");
		localStorage.removeItem("apellidos");
		$.mobile.changePage("#login",{transition: "slide",reverse:true});
	});

	$("#btagregar").click(function(){
		var userform = $('#userform').serializeArray();
		var userFormObject = {};
		
		$.each(userform,function(i, v) {
			userFormObject[v.name] = v.value;
		});

		var jsonform = JSON.stringify(userFormObject,null,2);

		$.mobile.loadingMessageTextVisible = true;
		$.mobile.showPageLoadingMsg("c", "Enviando datos espere...", false);
		
		$.ajax({
			url: './clases/appcontroller.php',
			method: 'get',
			data: {jsonform:jsonform,option: "newuser",estado_: true, opt:2},
			dataType:'json',
			success: function(data) {
				console.log("data:",data);
				$.mobile.hidePageLoadingMsg();
				if (data[0].status) {
					if (data[0].status=="Ok") {
						//cerramos el popup y regresamos al login
						$("#formsuccess").popup("open").on({
							popupafterclose: function() {
								$('#userform').get(0).reset();
								$.mobile.changePage("#login",{transition: "slide",reverse:true});
							}
						});
					}else{
						//console.log("Estado:",data[0].status);
						$("#formerror").popup("open");
					}
				}else{
					//console.log("No se recibieron los datos adecuados");
					$("#formerror").popup("open");
				};
			},
			error:function(){
				console.log("Error");
			}
		});
	});

	$("#btcancelar").click(function(){
		console.log("bt cancelar");
		$('#userform').get(0).reset();
		$.mobile.changePage("#login",{transition: "slide",reverse:true});
	});

	$('#histok').click(function() {
		var userID="";
		if(localStorage.getItem("userID")){
			userID=localStorage.getItem("userID");
		}
		if (userID==""||userID=="unknown") {
			$("#histerror").popup("open");
		}else{
			$.mobile.loadingMessageTextVisible = true;
			$.mobile.showPageLoadingMsg("c", "Enviando datos espere...", false);
			$.ajax({
				url: './clases/appcontroller.php',
				method: 'get',
				data: {option: "deletemsg", userid:userID, estado_: true, opt:2},
				dataType:'json',
				success: function(data) {
					console.log("data:",data);
					if (data[0].status) {
						if (data[0].status=="Ok") {
							console.log("status ok");
							$.mobile.hidePageLoadingMsg();
						}else{
							//console.log("Estado:",data[0].status);
							$("#histerror").popup("open");
						}
					}else{
						//console.log("No se recibieron los datos adecuados");
						$("#histerror").popup("open");
					};
				},
				error:function(){
					console.log("Error");
				}
			});
		}
	});

	$('#accountok').click(function() {
		var userID="";
		if(localStorage.getItem("userID")){
			userID=localStorage.getItem("userID");
		}
		if (userID==""||userID=="unknown") {
			$("#accounterror").popup("open");
		}else{
			$.mobile.loadingMessageTextVisible = true;
			$.mobile.showPageLoadingMsg("c", "Enviando datos espere...", false);
			$.ajax({
				url: './clases/appcontroller.php',
				method: 'get',
				data: {option: "delete", userid:userID, estado_: true, opt:2},
				dataType:'json',
				success: function(data) {
					console.log("data:",data);
					if (data[0].status) {
						if (data[0].status=="Ok") {
							//cerramos el popup y regresamos al login
							$.mobile.hidePageLoadingMsg();
							console.log("status ok");
							$("#accountsuccess").popup("open").on({
								popupafterclose: function() {
									console.log("cuenta eliminada listo");
									setTimeout( function(){
										$("#logout").click();
									},2000);
								}
							});
						}else{
							//console.log("Estado:",data[0].status);
							$("#accounterror").popup("open");
						}
					}else{
						//console.log("No se recibieron los datos adecuados");
						$("#accounterror").popup("open");
					};
				},
				error:function(){
					console.log("Error");
				}
			});
		}
	});

	$('#newsmsg').on('pageshow',function(event){
		var userID="";
		if(localStorage.getItem("userID")){
			userID=localStorage.getItem("userID");
		}
		if (userID==""||userID=="unknown") {
			$("#msgerror").popup("open");
		}else{
			$.mobile.loadingMessageTextVisible = true;
			$.mobile.showPageLoadingMsg("c", "Enviando datos espere...", false);
			$.ajax({
				url: './clases/appcontroller.php',
				method: 'get',
				data: {option: "getmsg", userid:userID, estado_: true, opt:1},
				dataType:'json',
				success: function(data) {
					console.log("data:",data);					
					$("#msglist").html('');
					if (data[0].mensaje) {
						if (data[0].mensaje=="noMsg") {
							$("#msglist").html('<li data-role="list-divider"></li><li><a href="#"><h2>No hay mensajes nuevos...</h2><p>Presiona para recargar</p></a></li>');
						}else{
							$.each(data,function(index,value) {
								$("#msglist").append('<li data-role="list-divider"></li><li><a href="#"><h2>Mensaje enviado</h2><p>'+data[index].mensaje+'<br><strong>Ubicacion: </strong>'+data[index].localizacion+'</p><p class="ui-li-aside"><strong>Fecha: '+data[index].fecha+'</strong></p></a></li>');
					    	});
						}
					}else{
						$("#msglist").html('<li data-role="list-divider"></li><li><a href="#"><h2>No hay mensajes nuevos...</h2><p>Presiona para recargar</p></a></li>');
						$("#msgerror").popup("open");
					}
					$("#msglist").listview();
			    	$.mobile.hidePageLoadingMsg();
				    $("#msglist").listview("refresh");		
				},
				error:function(){
					console.log("Error al recibir datos");
					$("#msglist").html('<li data-role="list-divider"></li><li><a href="#"><h2>No hay mensajes nuevos...</h2><p>Presiona para recargar</p></a></li>');
					$("#msglist").listview();
					$.mobile.hidePageLoadingMsg();
				    $("#msglist").listview("refresh");	
				}
			});
		}
	});
	
	$('#mappage').on('pageshow',function(event){
		var userID="";
		var usuario="unknown";
		if(localStorage.getItem("userID")){
			userID=localStorage.getItem("userID");
			usuario=localStorage.getItem("nombres");
			usuario+=' '+localStorage.getItem("apellidos");

			if (userID==""||userID=="unknown") {
				console.log("error de userID");
			}else if (usuario==""||usuario=="unknown") {
				console.log("error de id de usuario");
			}else{
				console.log("asignando id de usuario");
				$("#username").html("Usuario [ "+usuario+" ]");
			}
			setTimeout(function(){
				console.log("cargando mapa");
				currentPosition();				
			}, 500);
		}
	});

/********************************************************************************/
	if(jQuery.browser.mobile){
		console.log('Estas usando un dispositivo movil!');
	}else{
		console.log('NO estas usando un dispositivo movil!');
	}
	if(window.navigator.onLine){
		console.log('Estas Conectado a internet!');			
	}else{
		console.log('NO estas Conectado a internet!');		
	}
});	
