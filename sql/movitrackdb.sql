-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-06-10 22:16:59
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table movitrackdb.mensajes
CREATE TABLE IF NOT EXISTS `mensajes` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(3) NOT NULL DEFAULT '0',
  `mensaje` varchar(140) COLLATE latin1_spanish_ci DEFAULT NULL,
  `localizacion` varchar(70) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fecha` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tipo_novedad` varchar(15) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`id_usuario`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Data exporting was unselected.


-- Dumping structure for table movitrackdb.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(15) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `nombres` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `direccion` varchar(35) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(40) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
