<?php
class usuariosClass {

	/**
	 * [loginUser realiza el login del usuario]
	 * @param  [string] $id_user [identificacion del usuario]
	 * @return [json] $jsondata [informe de estado de la operacion realizada]
	 */
	function loginUser($id_user){
		//trae el estado del login y el ID del usuario en un json
		$jsondata = array();
		$estado=$_GET['estado_'];
		if($estado){
			$UID = preg_replace("/[^0-9]/", "", $id_user);
			$sqlquery="SELECT id, nombres, apellidos FROM `usuarios` WHERE identificacion=".$UID;
			$result = mysql_query($sqlquery);	
			if($row = mysql_fetch_assoc($result)){
				$jsondata[0]['status'] = 'Ok';
				$jsondata[1]['userID'] = 'MAPPID'.$row['id'];
				$jsondata[2]['nombres'] = $row['nombres'];
				$jsondata[3]['apellidos'] = $row['apellidos'];
			}else{
				$jsondata[0]['status'] = 'Error';
			}
			return  json_encode($jsondata);
		}else{
			$this->Error(false);
		}
	}

	/**
	 * [newUser registra un nuevo usuario]
	 * @param  [json] $newuser [json que contiene los datos del usuario]
	 * @return [json] $jsondata [informe de estado de la operacion realizada]
	 */
	function newUser($newuser){
		$jsonform = array();
		$jsondata = array();
		$jsonform=json_decode($newuser,true);
		$i=0;
		$query_save="INSERT INTO `usuarios` (`identificacion`, `nombres`, `apellidos`,`direccion`,`telefono`,`email`) VALUES (";
		
		foreach($jsonform as $key => $value) {
			$query_save.='"'.$value.'"';
			if($i<5){
				$query_save.=',';
			}else{
				$query_save.=')';
			}
			$i++;
		}

		$result = mysql_query($query_save);	
		if ($result) {
			$jsondata[0]['status'] = 'Ok';
		}else{
			$jsondata[0]['status'] = 'Error';
		}
		return  json_encode($jsondata);
	}

	/**
	 * [deleteUser Elimina una cuenta de usuario]
	 * @param  [string] $id_user [id del usuario]
	 * @return [json] $jsondata [informe de estado de la operacion realizada]
	 */
	function deleteUser($id_user){
		$jsondata = array();
		$UID = preg_replace("/[^0-9]/", "", $id_user);
		$query_del="DELETE FROM `usuarios` WHERE id=".$UID;
		$result = mysql_query($query_del);			
		if ($result) {
			$jsondata[0]['status'] = 'Ok';
		}else{
			$jsondata[0]['status'] = 'Error';
		}								
		return  json_encode($jsondata);
	}

	/**
	 * [deleteMsgUser Elimina todos los mensajes de una cuenta de usuario]
	 * @param  [string] $id_user [id del usuario]
	 * @return [json] $jsondata [informe de estado de la operacion realizada]
	 */
	function deleteMsgUser($id_user){
		$jsondata = array();
		$UID = preg_replace("/[^0-9]/", "", $id_user);
		$query_del="DELETE FROM `mensajes` WHERE id_usuario=".$UID;
		$result = mysql_query($query_del);			
		if ($result) {
			$jsondata[0]['status'] = 'Ok';
		}else{
			$jsondata[0]['status'] = 'Error';
		}								
		return  json_encode($jsondata);
	}

	function Error($default_Error){
		if ($default_Error){
			echo "<div class='status error'>Ha ocurrido un Error Inesperado.</div>";
		}else{
			echo "<div class='status error'>Ha ocurrido un Error al enviar los datos.</div>";
		}
		$estado=false;			
		return false;
	}

	/**
	 * [Page_index redirecciona a un url especifica]
	 * @param [string] $page [url del recurso]
	 */
	function Page_index($page){
		header("Location: ".$page."");
		//return true;
	}
	
	/**
	 * [index_content funcion ppal de la app controla los parametros a utilizar y el llamado a las clases]
	 * @param  [string] $option  [parametro que indica que funcion usar]
	 * @param  [bool] $estado_ [define si la operacion a realizar es valida]
	 * @return [json] [json con el recurso requerido]
	 */
	function index_content($option,$estado_){
		if ($_GET['option']){
			switch ($option) {
				case 'login' :
					if ((!$_GET['logced']) && (!$_GET['estado_'])){
						$this->Error(true);
					}else{
						echo $this->loginUser($_GET['logced']);
					}							
				break;
				case 'newuser' :
					if (!$_GET['jsonform']){
						$this->Error(true);
					}else{
						echo $this->newUser($_GET['jsonform']);
					}							
				break;
				case 'delete' :
					if ((!$_GET['userid']) && (!$_GET['estado_'])){
						$this->Error(true);
					}else{
						echo $this->deleteUser($_GET['userid']);
					}							
				break;
				case 'deletemsg' :
					if ((!$_GET['userid']) && (!$_GET['estado_'])){
						$this->Error(true);
					}else{
						echo $this->deleteMsgUser($_GET['userid']);
					}							
				break;
				default : // Todos
					$this->Page_index("../index.html");
					break;
			}//switch    
		}else{
			$this->Page_index("../index.html");
		}//if ($_GET)
	}//index_content
}
?>
