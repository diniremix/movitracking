<?php
date_default_timezone_set("America/Bogota");
class ConnectClass {
		var $conn;
		var $fullconn;
	 	var $host;
	 	var $username;
	 	var $password;
	 	var $db;

	/**
	* [managerClass Constructor de la clase ConnectClass]
	*/
	function ConnectClass(){
	 		$this->host='localhost';
	 		$this->username='root';
	 		$this->password='';
			$this->db='movitrackdb';
	}
	
	/**
	 * [connect realiza la conexion al servidor]
	 * @return [obj] $conn [objeto de conexion para la aplicacion]
	 */
	function connect(){
		$this->conn=mysql_connect($this->host,$this->username,$this->password) or die( mysql_error());
		$this->fullconn=mysql_select_db($this->db,$this->conn) or die( mysql_error());
		return $this->fullconn;
	}
}
?>
